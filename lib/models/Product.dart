class Product {
  String nombre;
  String codigo;
  List<ProductImage> imagenes;
  double precioVenta;
  int marcaId;

  Product(
      {required this.nombre,
      required this.codigo,
      required this.imagenes,
      required this.precioVenta,
      required this.marcaId});
}

class ProductImage {
  String imagenUrl;
  ProductImage({
    required this.imagenUrl,
  });
}
