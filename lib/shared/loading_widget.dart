import 'package:flutter/material.dart';

import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class LoadingFullPageWidget extends StatelessWidget {
  const LoadingFullPageWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: double.maxFinite,
      child: Padding(
        padding: const EdgeInsets.all(60.0),
        child: SleekCircularSlider(
            appearance: const CircularSliderAppearance(),
            onChange: (double value) {
              print(value);
            }),
      ),
    );
  }
}
