import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../pages/login_page.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 1), () {
      // Esperar 4 segundos
      //Navigator.pushReplacement(
      //    context, MaterialPageRoute(builder: (_) => const LoginPage()));
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => const LoginPage()));
    });

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: SizedBox(
        width: double.maxFinite,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(),
            Hero(
              tag: 'logo-white',
              child: Image.asset('assets/images/logos/logo-withe.png'),
            ),
            const SizedBox(
              height: 100,
            ),
          ],
        ),
      ),
    );
  }
}
