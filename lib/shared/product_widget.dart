import 'package:flutter/material.dart';

class ProductWidget extends StatelessWidget {
  final dynamic product;
  const ProductWidget({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Card(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                const Text('Nombre del producto'),
                Image.asset(
                  'assets/images/logos/logo-google.png',
                  width: 50,
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: const [Text('Status'), Text('call service')],
            ),
          ],
        ),
      ),
    );
  }
}
