class Marca {
  int id;
  String nombre;
  String logoUrl;

  Marca({
    required this.id,
    required this.nombre,
    required this.logoUrl,
  });

  static Marca fromMap(Map<String, dynamic> map) {
    return Marca(
      id: map['id'],
      nombre: map['nombre'],
      logoUrl: map['logoUrl'],
    );
  }
}
