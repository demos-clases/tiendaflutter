class Producto {
  int id;
  int marcaId;
  String nombre;
  String codigo;
  double precioVenta;
  String sku;
  List<ImagenProducto> imagenes;

  Producto({
    required this.id,
    required this.marcaId,
    required this.nombre,
    required this.codigo,
    required this.precioVenta,
    required this.sku,
    required this.imagenes,
  });

  static Producto fromMap(Map<String, dynamic> map) {
    return Producto(
      id: map['id'],
      marcaId: map['marcaId'],
      nombre: map['nombre'],
      codigo: map['codigo'],
      precioVenta: map['precioVenta'],
      sku: map['sku'],
      imagenes: map['imagenes']
          .map<ImagenProducto>((img) => ImagenProducto.fromMap(img))
          .toList(),
    );
  }
}

class ImagenProducto {
  int id;
  int productoId;
  String imagenUrl;
  ImagenProducto({
    required this.id,
    required this.productoId,
    required this.imagenUrl,
  });

  static ImagenProducto fromMap(Map<String, dynamic> map) {
    return ImagenProducto(
      id: map['id'],
      productoId: map['productoId'],
      imagenUrl: map['imagenUrl'],
    );
  }
}
