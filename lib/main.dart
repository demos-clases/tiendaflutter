import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tienda3n1/pages/home_page.dart';
import 'package:tienda3n1/pages/profile_page.dart';
import 'package:tienda3n1/pages/tab_page.dart';
import 'package:tienda3n1/shared/splash_page.dart';

import 'pages/menu_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MenuPage(),
    );
  }
}
