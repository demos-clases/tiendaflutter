import 'dart:io';

import 'package:tienda3n1/entities/marca.dart';

import '../../entities/producto.dart';
import 'package:dio/dio.dart';

class ProductoService {
  static Future<List<Producto>> list() async {
    Dio dio = Dio();
    var response = await dio
        .get('https://contistoredemo2.azurewebsites.net/api/v1/products');
    List<Producto> productos =
        response.data.map<Producto>((json) => Producto.fromMap(json)).toList();
    return productos;
  }

  static Future<List<Marca>> marcas() async {
    Dio dio = Dio();
    var response = await dio
        .get('https://contistoredemo2.azurewebsites.net/api/v1/marcas');
    List<Marca> marcas =
        response.data.map<Marca>((json) => Marca.fromMap(json)).toList();
    return marcas;
  }
}
