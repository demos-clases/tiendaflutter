import 'package:flutter/material.dart';

import '../shared/product_widget.dart';

class PedidosPartial extends StatelessWidget {
  const PedidosPartial({super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: ListView.builder(
            itemCount: 10,
            itemBuilder: (ctx, index) {
              return ProductWidget(
                product: index,
              );
            }));
  }
}
