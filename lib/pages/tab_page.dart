import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:tienda3n1/pages/pedidos_partial.dart';

import 'ordenes_partial.dart';

class TabPage extends StatelessWidget {
  const TabPage({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        //appBar: TabBar(tabs: [Text('Pedido'), Text('Ordenes')]),
        appBar: AppBar(
          bottom: const TabBar(tabs: [Text('Pedido'), Text('Ordenes')]),
        ),
        body: const TabBarView(children: [
          PedidosPartial(),
          OrdenesPartial(),
        ]),
      ),
    );
  }
}
