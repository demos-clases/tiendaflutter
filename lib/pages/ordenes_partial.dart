import 'package:flutter/material.dart';

import '../shared/product_widget.dart';

class OrdenesPartial extends StatelessWidget {
  const OrdenesPartial({super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: ListView.builder(
            itemCount: 3,
            itemBuilder: (ctx, index) {
              return ProductWidget(
                product: index,
              );
            }));
  }
}
