import 'package:flutter/material.dart';
import 'package:tienda3n1/entities/state_load_enum.dart';

import '../components/gallery_comp.dart';
import '../entities/producto.dart';
import '../servicios/productos/productos.service.dart';
import '../shared/loading_widget.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Producto> products = [];
  StateLodadEnum state = StateLodadEnum.loading;
  @override
  Widget build(BuildContext context) {
    ProductoService.list().then((value) {
      setState(() {
        state = StateLodadEnum.done;
        products = value;
      });
    }).onError((error, stackTrace) {
      setState(() {
        state = StateLodadEnum.error;
      });
      print(error);
    });

    return Scaffold(
      appBar: AppBar(
        title: const Text("Mi applicación"),
      ),
      body: Container(
        color: Colors.blue[100],
        child: cargaDinamica(state),
      ),
    );
  }

  Widget cargaDinamica(StateLodadEnum state) {
    if (state == StateLodadEnum.loading) {
      return const LoadingFullPageWidget();
    } else if (state == StateLodadEnum.error) {
      return const Text('Tubimos error');
    } else if (state == StateLodadEnum.none) {
      return const Text('___');
    } else {
      return GalleryComponent(
        products: products,
      );
    }
  }
}
