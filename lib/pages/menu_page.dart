import 'package:flutter/material.dart';

import 'home_page.dart';
import 'login_page.dart';
import 'profile_page.dart';
import 'tab_page.dart';

class MenuPage extends StatelessWidget {
  const MenuPage({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        // ignore: avoid_unnecessary_containers
        body: Container(
            child: TabBarView(children: [
          const ProfilePage(),
          const TabPage(),
          HomePage(),
          const LoginPage(),
        ])),
        bottomNavigationBar: const BottomAppBar(
          // #2d2d2d
          color: Color(0xFF2d2d2d),
          child: TabBar(tabs: [
            MenuBarIcon(icon: Icons.person_outline_rounded, title: 'Perfil'),
            MenuBarIcon(icon: Icons.bike_scooter_outlined, title: 'Pedidos'),
            MenuBarIcon(icon: Icons.schedule_outlined, title: 'Historial'),
            MenuBarIcon(icon: Icons.paid_outlined, title: 'Caja'),
          ]),
        ),
      ),
    );
  }
}

class MenuBarIcon extends StatelessWidget {
  final IconData icon;
  final String title;
  const MenuBarIcon({super.key, required this.icon, required this.title});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(
          icon,
          size: 32,
        ),
        Text(title)
      ],
    );
  }
}
