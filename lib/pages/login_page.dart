import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: SizedBox(
        width: double.maxFinite,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Hero(
                tag: 'logo-white',
                child: Image.asset('assets/images/logos/logo-withe.png'),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: const TextField(
                  decoration: InputDecoration(
                      icon: Icon(Icons.person_outline, color: Colors.black26),
                      focusColor: Colors.red,
                      labelText: 'Usuarios',
                      floatingLabelStyle: TextStyle(color: Color(0xFFe6017e)),
                      // #e6017e
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFe6017e)))),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: const TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                      icon: Icon(Icons.lock_outline, color: Colors.black26),
                      focusColor: Colors.red,
                      labelText: 'Contraseña',
                      floatingLabelStyle: TextStyle(color: Color(0xFFe6017e)),
                      // #e6017e
                      focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFe6017e)))),
                ),
              ),
              const SizedBox(
                height: 60,
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 80),
                width: double.maxFinite,
                child: OutlinedButton(
                  onPressed: () {},
                  // ignore: sort_child_properties_last
                  child: const Expanded(
                    child: Text(
                      'INGRESAR',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  style: OutlinedButton.styleFrom(
                    backgroundColor: const Color(0xFFe6017e),
                    foregroundColor: Colors.white,
                  ),
                ),

                //decoration: BoxDecoration(color: Color(0xFFe6017e)),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 80),
                width: double.maxFinite,
                child: OutlinedButton(
                  onPressed: () {},
                  child: Row(
                    children: [
                      Image.asset(
                        'assets/images/logos/logo-google.png',
                        width: 26,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text('INGRESA CON GOOGLE'),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 100,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
