import 'package:flutter/material.dart';
import 'package:tienda3n1/components/product_comp.dart';
import 'package:tienda3n1/entities/producto.dart';
import 'package:tienda3n1/servicios/productos/productos.service.dart';

import '../entities/marca.dart';

class GalleryComponent extends StatefulWidget {
  List<Producto> products;

  GalleryComponent({super.key, required this.products});

  @override
  State<GalleryComponent> createState() => _GalleryComponentState();
}

class _GalleryComponentState extends State<GalleryComponent> {
  List<Marca> marcas = [];

  @override
  Widget build(BuildContext context) {
    ProductoService.marcas().then(((value) {
      setState(() {
        marcas = value;
      });
    })).onError((error, stackTrace) {
      print('ERROR');
    });

    return Container(
      padding: const EdgeInsets.all(12),
      child: Column(
        children: [
          // Titulo
          const Text(
            "Productos",
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
          // Caterias
          SizedBox(
              height: 60,
              child: ListView.separated(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: marcas.length,
                itemBuilder: (ctx, i) {
                  return Image.network(
                    'https://contistoredemo2.azurewebsites.net${marcas[i].logoUrl}',
                    height: 60,
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return const SizedBox(
                    width: 10,
                  );
                },
              )),
          Expanded(
            child: GridView.count(
              crossAxisCount: 2,
              children: widget.products.map<Widget>((item) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ProductComponent(
                    product: item,
                  ),
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  }
}
