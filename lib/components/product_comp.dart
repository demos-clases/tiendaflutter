import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../../entities/producto.dart';

// ignore: must_be_immutable
class ProductComponent extends StatelessWidget {
  Producto product;
  ProductComponent({super.key, required this.product});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          )),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("S/ ${product.precioVenta}"),
              Text(product.codigo.toString()),
            ],
          ),
          SizedBox(
            height: 110,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: product.imagenes.length,
                itemBuilder: (ctx, i) {
                  var img = product.imagenes[i];
                  return Image.network(
                      'https://contistoredemo2.azurewebsites.net${img.imagenUrl}',
                      height: 110);
                }),
          ),
          Text(
            product.nombre,
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          Text(product.marcaId.toString())
        ],
      ),
    );
  }
}
